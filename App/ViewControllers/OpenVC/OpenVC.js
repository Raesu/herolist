

import React, { Component } from 'react';
import { View, Button, StyleSheet } from 'react-native';

class OpenVC extends Component {

    onButtonPress = () => {
        this.props.navigation.navigate('ListVC');
    }

    render() {
        return (
            <View style={styleGuide.containerView}>
                <Button title='Lets go!' onPress={this.onButtonPress}/>
            </View>
        );
    }
}

export default OpenVC;

const styleGuide = StyleSheet.create({
    containerView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
});



