


import React, { Component } from 'react';
import { View, Button, TextInput, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import HeroTableView from './HeroTableView';

class ListVC extends Component {

    static propTypes = {
        data: PropTypes.array.isRequired
    }

    constructor(props) {
        super(props);
        this.state = {
            newHeroName: ''
        }
    }

    textChanged = (text) => {
        this.setState({ newHeroName: text });
    }
    
    addNewHero = () => {
        this.props.addItem(this.state.newHeroName);
        this.setState({ newHeroName: '' })
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.inputContainer}>
                    <TextInput value={this.state.newHeroName} onChangeText={this.textChanged} style={styles.inputBar}/>
                    <Button title='Add' onPress={this.addNewHero}/>
                </View>
                <HeroTableView data={this.props.data}/>
            </View>
        );
    }

}

const mapStateToProps = (state) => ({
    data: state.root.data
})

const mapDispatchToProps = dispatch => {
    return {
      addItem: (name) => {
        dispatch(addItem(name))
      }
    }
}
  
export default connect(mapStateToProps, mapDispatchToProps)(ListVC);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        margin:10
    },
    inputContainer: {
        flexDirection: 'row',
        borderRadius: 10,
        borderWidth: 1,
        borderColor: 'black',
        padding: 5
    },
    inputBar: {
        flex: 1,
        marginRight: 10
    }
})


