


import React, { Component } from 'react';
import { StyleSheet, View, FlatList } from 'react-native';
import PropTypes from 'prop-types';
import HeroCell from './HeroCell';

class HeroTableView extends Component {

    static propTypes = {
        data: PropTypes.array.isRequired
    }

    keyExtractor = (item, index) => index;

    render() {
        const { data } = this.props
        
        return (
            <View style={styles.container}>
                <FlatList key={data.id} keyExtractor={this._keyExtractor} data={data} renderItem={({item, index}) => 
                    <HeroCell item={item}/>}
                />
            </View>
        );
  }
}

export default HeroTableView;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white'
  }
});
