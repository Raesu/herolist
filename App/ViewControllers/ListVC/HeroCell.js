


import React, { Component } from 'react';
import { Text, View, TouchableOpacity, AlertIOS } from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { changeName } from '../../Data/Actions'

class HeroCell extends Component {

    static propTypes = {
        item: PropTypes.object.isRequired,
    }

    wasPressed = () => {}

    wasLongPressed = () => {
        AlertIOS.prompt("Update Hero Name", null, handleSubmit(text), 'default', null, 'default');
        // this.setState({ modalVisible: true });
    }

    handleSubmit = (text) => {
        this.props.changeName(item.id, text);
    }

    render() {

        const { item } = this.props;

        return (
            <TouchableOpacity onPress={this.wasPressed} onLongPress={this.wasLongpressed}>
                <View style={{backgroundColor: 'white', height: 44, justifyContent: 'center'}}>
                    <Text style={{marginLeft: 8}}>{item.heroName}</Text>
                </View>
            </TouchableOpacity>
        );
    }
}

const mapStateToProps = (state) => ({
    data: state.root.data
})

const mapDispatchToProps = dispatch => {
    return {
      changeName: (id, name) => {
        dispatch(changeName(id, name));
      }
    }
}
  
export default connect(mapStateToProps, mapDispatchToProps)(HeroCell);

