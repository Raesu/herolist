


import OpenVC from './ViewControllers/OpenVC/OpenVC';
import ListVC from './ViewControllers/ListVC/ListVC'
import { createStackNavigator, createAppContainer } from 'react-navigation';


const AppNavigator = createStackNavigator({
    OpenVC: OpenVC,
    ListVC: ListVC
});


export default createAppContainer(AppNavigator);