


import { ADD_HERO, CHANGE_NAME } from './Actions'

const initialState = {
  data: [
    {
      id: '0',
      name: 'Reinhardt',
    },
    {
      id: '1',
      name: 'Lucio',
    },
    {
      id: '2',
      name: 'Soldier 76',
    }
  ]
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_HERO:
      return {
        ...state,
        data: [...state.data, action.item]
      }
    case CHANGE_NAME:
      const data = state.data
      data[action.id].name = action.name
      return {
        ...state,
        data: data
      }
    default:
      return state
  }
};

export default reducer

