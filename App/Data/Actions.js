

import _ from 'lodash';

export const ADD_HERO = 'ADD_HERO';
export const CHANGE_NAME = 'CHANGE_NAME';

export const addHero = (name) => ({
  type: ADD_HERO,
  item: {
    id: _.uniqueId('ow_'),
    name: name,
  }
});

export const changeName = (id, name) => ({
    type: CHANGE_NAME,
    item: {
      id,
      name: name,
    }
  });



