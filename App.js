


import React, {Component} from 'react';
import { createAppContainer } from 'react-navigation';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist';
import AppNavigator from './App/NavigationStack';
import { store, persistor } from './App/Data/DataStore';

const AppContainer = createAppContainer(AppNavigator);

class App extends Component {
    render() {
        return (
            <Provider store={store}>
                <PersistGate persistor={persistor}>
                    <AppContainer/>
                </PersistGate>
            </Provider>
        );
    }
}

export default App;





